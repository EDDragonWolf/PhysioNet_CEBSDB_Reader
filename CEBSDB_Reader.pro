TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

# WFDB have internal problem with database path, which defined before its building
# Current database path: /home/user/wfdb-10.5.24/build/database/
include(wfdb/wfdb.pri)

SOURCES += main.cpp
